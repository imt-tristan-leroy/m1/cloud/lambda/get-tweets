use std::collections::HashMap;
use std::str;

use aws_sdk_dynamodb::{model::AttributeValue, Client, Error as DynamoError};
use base64::{decode, encode};
use serde::{Deserialize, Serialize};

fn encode_cursor(key: HashMap<String, AttributeValue>) -> String {
    let s = as_string(key.get("channel_id"), &"".to_string())
        + "|"
        + &as_string(key.get("timestamp_utc_iso8601"), &"".to_string());
    encode(s)
}

fn decode_cursor(cursor: String) -> HashMap<String, AttributeValue> {
    let byte_cursor = decode(cursor).unwrap();
    let key = str::from_utf8(&byte_cursor).unwrap();
    let mut values = key.split("|");
    let mut map: HashMap<String, AttributeValue> = HashMap::new();

    map.insert(
        "channel_id".to_owned(),
        AttributeValue::S(values.next().unwrap().to_string()),
    );
    map.insert(
        "timestamp_utc_iso8601".to_owned(),
        AttributeValue::S(values.next().unwrap().to_string()),
    );
    map
}

fn as_string(val: Option<&AttributeValue>, default: &String) -> String {
    if let Some(v) = val {
        if let Ok(s) = v.as_s() {
            return s.to_owned();
        }
    }
    default.to_owned()
}

fn as_u8(val: Option<&AttributeValue>, default: u8) -> u8 {
    if let Some(v) = val {
        if let Ok(n) = v.as_n() {
            if let Ok(n) = n.parse::<u8>() {
                return n;
            }
        }
    }
    default
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Tweet {
    pub channel_id: String,
    pub likes: u8,
    pub message: String,
    pub username: String,
    pub timestamp_utc_iso8601: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PaginateTweet {
    pub tweets: Vec<Tweet>,
    pub cursor: Option<String>,
}

impl Tweet {
    pub fn new(
        channel_id: String,
        likes: u8,
        message: String,
        username: String,
        timestamp_utc_iso8601: String,
    ) -> Self {
        Tweet {
            channel_id,
            likes,
            message,
            username,
            timestamp_utc_iso8601,
        }
    }
}

impl From<&HashMap<String, AttributeValue>> for Tweet {
    fn from(value: &HashMap<String, AttributeValue>) -> Self {
        Tweet::new(
            as_string(value.get("channel_id"), &"".to_string()),
            as_u8(value.get("likes"), 0),
            as_string(value.get("message"), &"".to_string()),
            as_string(value.get("username"), &"".to_string()),
            as_string(value.get("timestamp_utc_iso8601"), &"".to_string()),
        )
    }
}

pub async fn get_tweets(
    client: &Client,
    table: &str,
    channel_id: &str,
    page_size: Option<i32>,
    cursor: Option<String>,
) -> Result<PaginateTweet, DynamoError> {
    let results = client
        .query()
        .table_name(table)
        .key_condition_expression("#channel_id = :channel_id")
        .expression_attribute_names("#channel_id", "channel_id")
        .expression_attribute_values(":channel_id", AttributeValue::S(channel_id.to_string()))
        .limit(match page_size {
            Some(size) => size,
            None => 10,
        })
        .set_exclusive_start_key(match cursor {
            Some(key) => Some(decode_cursor(key)),
            None => None,
        })
        .set_scan_index_forward(Some(false))
        .send()
        .await?;

    if let Some(items) = results.items {
        let tweets: Vec<Tweet> = items.iter().map(|v| v.into()).collect();
        Ok(PaginateTweet {
            tweets,
            cursor: match results.last_evaluated_key {
                None => None,
                Some(key) => Some(encode_cursor(key)),
            },
        })
    } else {
        Ok(PaginateTweet {
            tweets: vec![],
            cursor: None,
        })
    }
}
