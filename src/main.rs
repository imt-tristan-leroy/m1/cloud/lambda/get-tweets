pub mod tweet;

use std::str::FromStr;

use aws_sdk_dynamodb::Client;

use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};
use serde::Serialize;

use tweet::{get_tweets, Tweet};

#[derive(Serialize)]
struct ResponseBody {
    content: Vec<Tweet>,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let query = event.query_string_parameters();
    let page_size = query.first("pageSize");
    let cursor = query.first("cursor");
    let config = aws_config::load_from_env().await;

    let client = Client::new(&config);

    let tweets = get_tweets(
        &client,
        "dynamodb-all-messages",
        "rainbow",
        match page_size {
            Some(size) => Some(FromStr::from_str(size).unwrap()),
            None => None,
        },
        match cursor {
            Some(c) => Some(c.to_string()),
            None => None,
        },
    )
    .await?;

    let j = serde_json::to_string(&tweets)?;

    let resp = Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .header(
            "Access-Control-Allow-Origin",
            "http://s3-rainbow-front.s3-website-eu-west-1.amazonaws.com",
        )
        .header("Access-Control-Allow-Credentials", "true")
        .body(j.into())
        .map_err(Box::new)?;
    return Ok(resp);
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
